<?php 
include("config.php");
include("database.php"); 
include("control_panel_common.php");
include("control_panel_header.php");
Loader::register('./lib','RobThree\\Auth');
use \RobThree\Auth\TwoFactorAuth;
$tfa = new TwoFactorAuth('FFXIV Classic Server');
?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Seventh Umbral Server</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" />	
	<link rel="stylesheet" type="text/css" href="css/global.css" />	
</head>
<body>
<div class="edit">
    <?php

        $tfa = new TwoFactorAuth('FFXIV Classic Server');
        $row = GetUserInfo($g_databaseConnection, $g_userId);
        $secret = $tfa->createSecret();
        global $secret;
        echo '<br><img src="' . $tfa->getQRCodeImageAsDataUri($row["name"], $secret) . '"><br>' . chunk_split($secret, 4, ' ');
        $code = $tfa->getCode($secret);
        echo "<br>Scan the QR code or manually enter the code into your authenticator app.";
        echo '<br>At the time this page was loaded, the code <span style="color:#00c">' . $code . '</span> is what your app would display.';
        echo '<br>Reloading this page will generate a new authenticator secret and render the previous one null and void.';
        storeUserTFASecret($g_databaseConnection, $g_userId, $secret);
        ?>
        </div>
</body>
</html>